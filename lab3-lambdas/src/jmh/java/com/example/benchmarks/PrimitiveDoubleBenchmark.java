package com.example.benchmarks;

import com.example.operations.PrimitiveDoubleOperations;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.infra.Blackhole;

import java.util.Arrays;
import java.util.Random;
import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@Warmup(iterations = 10, time = 1)
@Measurement(iterations = 20, time = 1)
@Fork(1)
public class PrimitiveDoubleBenchmark {

    @State(Scope.Benchmark)
    public static class BenchmarkState {
        PrimitiveDoubleOperations operations;

        double[] numbers;

        public int size = 100000;

        @Setup(Level.Trial)
        public void setup() {
            numbers = new double[size];
            Random rand = new Random();

            for (int i = 0; i < size; i++) {
                numbers[i] = rand.nextDouble();
            }

            operations = new PrimitiveDoubleOperations(numbers);
        }
    }

    @Benchmark
    public void computeSumRandom(BenchmarkState state, Blackhole consumer) {
        consumer.consume(state.operations.computeSum());
    }

    @Benchmark
    public void computeAverageRandom(BenchmarkState state, Blackhole consumer) {
        consumer.consume(state.operations.computeAverage());
    }

    @Benchmark
    public void topTenPercentRandom(BenchmarkState state, Blackhole consumer) {
        consumer.consume(state.operations.getTopTenPercent());
    }

    @State(Scope.Benchmark)
    public static class SortedBenchmarkState extends BenchmarkState {
        @Setup(Level.Trial)
        public void setupSorted() {
            Arrays.sort(numbers);
            operations = new PrimitiveDoubleOperations(numbers);
        }
    }

    @Benchmark
    public void computeSumSorted(SortedBenchmarkState state, Blackhole consumer) {
        consumer.consume(state.operations.computeSum());
    }

    @Benchmark
    public void computeAverageSorted(SortedBenchmarkState state, Blackhole consumer) {
        consumer.consume(state.operations.computeAverage());
    }

    @Benchmark
    public void topTenPercentSorted(SortedBenchmarkState state, Blackhole consumer) {
        consumer.consume(state.operations.getTopTenPercent());
    }
}
