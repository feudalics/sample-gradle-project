package com.example.operations;

import java.util.Arrays;

public class PrimitiveDoubleOperations {

    private final double[] elems;

    public PrimitiveDoubleOperations(double[] elems) {
        this.elems = elems;
    }

    public double computeSum() {
        return Arrays.stream(elems).sum();
    }

    public double computeAverage() {
        return Arrays.stream(elems).average().orElse(-1);
    }

    public double[] getTopTenPercent() {
        int size = elems.length / 10;

        return Arrays.stream(elems).sorted().skip(elems.length - size).toArray();
    }
}
