package com.example.operations;

import java.util.List;

public interface StreamOperations<T> {
    T computeSum();

    double computeAverage();

    List<T> getTopTenPercent();
}
