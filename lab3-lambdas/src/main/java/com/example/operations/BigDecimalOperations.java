package com.example.operations;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

public class BigDecimalOperations implements StreamOperations<BigDecimal> {

    private final List<BigDecimal> elems;

    public BigDecimalOperations(List<BigDecimal> elems) {
        this.elems = elems;
    }

    @Override
    public BigDecimal computeSum() {
        return elems.stream().reduce(BigDecimal.valueOf(0), BigDecimal::add);
    }

    @Override
    public double computeAverage() {
        return elems.stream().mapToDouble(BigDecimal::doubleValue)
                .average().orElse(0.0);
    }

    @Override
    public List<BigDecimal> getTopTenPercent() {
        int size = elems.size() / 10;

        return elems.stream().sorted(BigDecimal::compareTo)
                .limit(size).collect(Collectors.toList());
    }
}
