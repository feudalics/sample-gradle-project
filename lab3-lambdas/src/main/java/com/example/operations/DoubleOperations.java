package com.example.operations;

import java.util.List;
import java.util.stream.Collectors;

public class DoubleOperations implements StreamOperations<Double> {

    private final List<Double> elems;

    public DoubleOperations(List<Double> elems) {
        this.elems = elems;
    }

    @Override
    public Double computeSum() {
        return elems.stream().reduce((double) 0, Double::sum);
    }

    @Override
    public double computeAverage() {
        return elems.stream().mapToDouble(Double::doubleValue)
                .average().orElse(0.0);
    }

    @Override
    public List<Double> getTopTenPercent() {
        int size = elems.size() / 10;

        return elems.stream().sorted(Double::compare)
                .limit(size).collect(Collectors.toList());
    }
}
