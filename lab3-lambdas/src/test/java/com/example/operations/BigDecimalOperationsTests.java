package com.example.operations;

import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class BigDecimalOperationsTests {

    @Test
    void testComputeSum() {
        List<BigDecimal> numbers = Arrays.asList(BigDecimal.valueOf(1.5), BigDecimal.valueOf(2.5), BigDecimal.valueOf(3.5));
        BigDecimalOperations operations = new BigDecimalOperations(numbers);

        BigDecimal sum = operations.computeSum();

        assertEquals(BigDecimal.valueOf(7.5), sum);
    }

    @Test
    void testComputeAverage() {
        List<BigDecimal> numbers = Arrays.asList(BigDecimal.valueOf(1.0), BigDecimal.valueOf(2.0), BigDecimal.valueOf(3.0));
        BigDecimalOperations operations = new BigDecimalOperations(numbers);

        double average = operations.computeAverage();

        assertEquals(2.0, average);
    }

    @Test
    void testComputeAverageWithEmptyList() {
        List<BigDecimal> emptyList = List.of();
        BigDecimalOperations operations = new BigDecimalOperations(emptyList);

        double average = operations.computeAverage();

        assertEquals(0.0, average);
    }

    @Test
    void testGetTopTenPercent() {
        List<BigDecimal> numbers = Arrays.asList(
                BigDecimal.valueOf(10), BigDecimal.valueOf(20), BigDecimal.valueOf(30),
                BigDecimal.valueOf(40), BigDecimal.valueOf(50), BigDecimal.valueOf(60),
                BigDecimal.valueOf(70), BigDecimal.valueOf(80), BigDecimal.valueOf(90),
                BigDecimal.valueOf(100), BigDecimal.valueOf(110), BigDecimal.valueOf(120)
        );
        BigDecimalOperations operations = new BigDecimalOperations(numbers);

        List<BigDecimal> topTenPercent = operations.getTopTenPercent();

        assertEquals(Arrays.asList(
                BigDecimal.valueOf(10), BigDecimal.valueOf(20)
        ), topTenPercent);
    }

    @Test
    void testGetTopTenPercentWithEmptyList() {
        List<BigDecimal> emptyList = List.of();
        BigDecimalOperations operations = new BigDecimalOperations(emptyList);

        List<BigDecimal> topTenPercent = operations.getTopTenPercent();

        assertEquals(emptyList, topTenPercent);
    }
}
