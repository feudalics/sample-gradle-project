package com.benchmarks;

import com.example.domain.Order;
import com.example.persistence.TreeSetBasedRepo;
import org.openjdk.jmh.annotations.BenchmarkMode;
import org.openjdk.jmh.annotations.Mode;
import org.openjdk.jmh.annotations.OutputTimeUnit;
import org.openjdk.jmh.annotations.Warmup;
import org.openjdk.jmh.annotations.Measurement;
import org.openjdk.jmh.annotations.Fork;
import org.openjdk.jmh.annotations.State;
import org.openjdk.jmh.annotations.Scope;
import org.openjdk.jmh.annotations.Setup;
import org.openjdk.jmh.annotations.Level;
import org.openjdk.jmh.annotations.Benchmark;
import org.openjdk.jmh.infra.Blackhole;

import java.util.concurrent.TimeUnit;

@BenchmarkMode(Mode.AverageTime)
@OutputTimeUnit(TimeUnit.NANOSECONDS)
@Warmup(iterations = 10, time = 1)
@Measurement(iterations = 20, time = 1)
@Fork(1)
@State(Scope.Benchmark)
public class TreeSetBenchmark {
    private final TreeSetBasedRepo repository = new TreeSetBasedRepo();

    private final int iterationCount = 1000;

    @Setup(Level.Iteration)
    public void setup() {
        for (int i = 0; i < iterationCount; i++) {
            repository.add(new Order(i, i * 2, i * 3));
        }
    }

    @Benchmark
    public void add() {
        for (int i = iterationCount; i < iterationCount + 100; i++) {
            repository.add(new Order(i, i * 2, i * 3));
        }
    }

    @Benchmark
    public void contains(Blackhole consumer) {
        for (int i = 0; i < iterationCount; i++) {
            boolean result = repository.contains(new Order(i, i * 2, i * 3));
            consumer.consume(result);
        }
    }

    @Benchmark
    public void remove() {
        for (int i = 0; i < iterationCount; i++) {
            repository.remove(new Order(i, i * 2, i * 3));
        }
    }
}
