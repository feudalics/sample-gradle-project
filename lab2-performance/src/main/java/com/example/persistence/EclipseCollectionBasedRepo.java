package com.example.persistence;

import com.example.domain.Order;
import org.eclipse.collections.api.map.MutableMap;
import org.eclipse.collections.impl.map.mutable.UnifiedMap;

public class EclipseCollectionBasedRepo implements InMemoryRepository<Order> {
    private final MutableMap<Integer, Order> map;

    public EclipseCollectionBasedRepo() {
        this.map = new UnifiedMap<>();
    }

    @Override
    public void add(Order order) {
        map.put(order.getId(), order);
    }

    @Override
    public boolean contains(Order order) {
        return map.contains(order);
    }

    @Override
    public void remove(Order order) {
        map.remove(order.getId());
    }
}
