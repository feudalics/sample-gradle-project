package com.example.persistence;

import com.example.domain.Order;

import java.util.HashSet;
import java.util.Set;

public class HashSetBasedRepo implements InMemoryRepository<Order> {
    private final Set<Order> set;

    public HashSetBasedRepo() {
        this.set = new HashSet<>();
    }

    @Override
    public void add(Order order) {
        set.add(order);
    }

    @Override
    public boolean contains(Order order) {
        return set.contains(order);
    }

    @Override
    public void remove(Order order) {
        set.remove(order);
    }
}
