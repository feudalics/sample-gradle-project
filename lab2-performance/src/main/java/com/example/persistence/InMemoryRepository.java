package com.example.persistence;

public interface InMemoryRepository<T> {
    void add(T t);

    boolean contains(T t);

    void remove(T t);
}
