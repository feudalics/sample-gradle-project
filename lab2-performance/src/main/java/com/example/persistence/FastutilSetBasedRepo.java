package com.example.persistence;

import com.example.domain.Order;
import it.unimi.dsi.fastutil.objects.ObjectArrayList;

public class FastutilSetBasedRepo implements InMemoryRepository<Order> {
    private final ObjectArrayList<Order> set;

    public FastutilSetBasedRepo() {
        set = new ObjectArrayList<>();
    }

    @Override
    public void add(Order order) {
        set.add(order);
    }

    @Override
    public boolean contains(Order order) {
        return set.contains(order);
    }

    @Override
    public void remove(Order order) {
        set.remove(order);
    }
}
