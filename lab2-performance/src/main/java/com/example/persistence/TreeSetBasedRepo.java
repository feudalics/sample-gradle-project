package com.example.persistence;

import com.example.domain.Order;

import java.util.Set;
import java.util.TreeSet;

public class TreeSetBasedRepo implements InMemoryRepository<Order> {
    private final Set<Order> set;

    public TreeSetBasedRepo() {
        this.set = new TreeSet<>();
    }

    @Override
    public void add(Order order) {
        set.add(order);
    }

    @Override
    public boolean contains(Order order) {
        return set.contains(order);
    }

    @Override
    public void remove(Order order) {
        set.remove(order);
    }
}
