package com.example.persistence;

import com.example.domain.Order;

import java.util.ArrayList;
import java.util.List;

public class ArrayListBasedRepo implements InMemoryRepository<Order> {
    private final List<Order> list;

    public ArrayListBasedRepo() {
        list = new ArrayList<>();
    }

    @Override
    public void add(Order order) {
        list.add(order);
    }

    @Override
    public boolean contains(Order order) {
        return list.contains(order);
    }

    @Override
    public void remove(Order order) {
        list.remove(order);
    }
}
