package com.example.persistence;

import com.example.domain.Order;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ConcHashMapBasedRepo implements InMemoryRepository<Order> {
    private final Map<Integer, Order> map;

    public ConcHashMapBasedRepo() {
        this.map = new ConcurrentHashMap<>();
    }

    @Override
    public void add(Order order) {
        map.put(order.getId(), order);
    }

    @Override
    public boolean contains(Order order) {
        return map.containsKey(order.getId());
    }

    @Override
    public void remove(Order order) {
        map.remove(order.getId());
    }
}
