package com.example;

import com.example.operations.Operation;
import com.example.operations.Addition;
import com.example.operations.Subtraction;
import com.example.operations.Multiplication;
import com.example.operations.Division;

public class OperationFactory {
    public static Operation getOperation(String operation) {
        switch (operation) {
            case "add":
                return new Addition();
            case "subtract":
                return new Subtraction();
            case "multiply":
                return new Multiplication();
            case "divide":
                return new Division();
            default:
                throw new IllegalArgumentException("Invalid operation");
        }
    }
}
