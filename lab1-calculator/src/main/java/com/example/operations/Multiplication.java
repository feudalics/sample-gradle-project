package com.example.operations;

import java.util.List;

public class Multiplication implements Operation {
    @Override
    public double calculate(List<Double> operands) {
       return operands.stream()
                .mapToDouble(Double::doubleValue)
                .reduce(1, (acc, operand) -> acc * operand);
    }
}
