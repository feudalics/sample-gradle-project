package com.example.operations;

import java.util.List;

public class Subtraction implements Operation {
    @Override
    public double calculate(List<Double> operands) {
        if (operands.isEmpty()) {
            return 0;
        }

        return operands.stream()
                .mapToDouble(Double::doubleValue)
                .reduce((first, second) -> first - second)
                .getAsDouble();
    }
}
