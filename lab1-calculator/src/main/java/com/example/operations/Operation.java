package com.example.operations;

import java.util.List;

public interface Operation {
    double calculate(List<Double> operands);
}
