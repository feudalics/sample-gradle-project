package com.example.operations;

import java.util.List;

public class Division implements Operation {
    @Override
    public double calculate(List<Double> operands) {
        return operands.stream()
            .mapToDouble(Double::doubleValue)
            .reduce((accumulator, operand) -> {
                if (operand == 0) {
                    throw new ArithmeticException("Division by zero is not allowed");
                }
                return accumulator / operand;
            })
            .orElseThrow(() -> new ArithmeticException("No values to divide"));
    }
}
