package com.example.operations;

import java.util.List;

public class Addition implements Operation {
    @Override
    public double calculate(List<Double> operands) {
        return operands.stream()
                .mapToDouble(Double::doubleValue)
                .sum();
    }
}
