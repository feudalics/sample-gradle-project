package com.example;

import com.example.operations.Operation;

import java.util.List;

public class Calculator {
    private final Operation operation;

    private final List<Double> operands;

    public Calculator(Operation operation, List<Double> operands) {
        this.operation = operation;
        this.operands = operands;
    }

    public double getResult() {
        return operation.calculate(operands);
    }
}
