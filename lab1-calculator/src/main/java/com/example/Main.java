package com.example;

import com.example.operations.Operation;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Operation operation = OperationFactory.getOperation(args[0]);

        List<Double> operands = new ArrayList<>();
        for (int i = 1; i < args.length; i++) {
            operands.add(Double.parseDouble(args[i]));
        }

        Calculator calculator = new Calculator(operation, operands);
        System.out.println(calculator.getResult());
    }
}