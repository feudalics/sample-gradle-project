package com.example;

import com.example.operations.Operation;
import com.example.operations.Addition;
import com.example.operations.Subtraction;
import com.example.operations.Multiplication;
import com.example.operations.Division;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class OperationFactoryTest {

    @Test
    public void testGetAdditionOperation() {
        Operation operation = OperationFactory.getOperation("add");
        Assertions.assertEquals(Addition.class, operation.getClass());
    }

    @Test
    public void testGetSubtractionOperation() {
        Operation operation = OperationFactory.getOperation("subtract");
        Assertions.assertEquals(Subtraction.class, operation.getClass());
    }

    @Test
    public void testGetMultiplicationOperation() {
        Operation operation = OperationFactory.getOperation("multiply");
        Assertions.assertEquals(Multiplication.class, operation.getClass());
    }

    @Test
    public void testGetDivisionOperation() {
        Operation operation = OperationFactory.getOperation("divide");
        Assertions.assertEquals(Division.class, operation.getClass());
    }

    @Test
    public void testGetInvalidOperation() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> {
            OperationFactory.getOperation("invalid");
        });
    }
}
