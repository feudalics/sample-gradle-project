package com.example;

import com.example.operations.Addition;
import com.example.operations.Division;
import com.example.operations.Multiplication;
import com.example.operations.Subtraction;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

public class CalculatorTest {

    @Test
    public void testAddition() {
        List<Double> operands = Arrays.asList(5.0, 3.0);
        Calculator calculator = new Calculator(new Addition(), operands);
        double result = calculator.getResult();
        Assertions.assertEquals(8.0, result, 0.001);
    }

    @Test
    public void testSubtraction() {
        List<Double> operands = Arrays.asList(5.0, 3.0);
        Calculator calculator = new Calculator(new Subtraction(), operands);
        double result = calculator.getResult();
        Assertions.assertEquals(2.0, result, 0.001);
    }

    @Test
    public void testMultiplication() {
        List<Double> operands = Arrays.asList(5.0, 3.0);
        Calculator calculator = new Calculator(new Multiplication(), operands);
        double result = calculator.getResult();
        Assertions.assertEquals(15.0, result, 0.001);
    }

    @Test
    public void testDivision() {
        List<Double> operands = Arrays.asList(6.0, 3.0);
        Calculator calculator = new Calculator(new Division(), operands);
        double result = calculator.getResult();
        Assertions.assertEquals(2.0, result, 0.001);
    }
}
