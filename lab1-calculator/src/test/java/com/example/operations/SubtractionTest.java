package com.example.operations;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

public class SubtractionTest {

    @Test
    public void testSubtractionWithPositiveNumbers() {
        Subtraction subtraction = new Subtraction();
        List<Double> operands = Arrays.asList(5.0, 3.0);
        double result = subtraction.calculate(operands);
        Assertions.assertEquals(2.0, result, 0.001);
    }

    @Test
    public void testSubtractionWithNegativeNumbers() {
        Subtraction subtraction = new Subtraction();
        List<Double> operands = Arrays.asList(-5.0, -3.0);
        double result = subtraction.calculate(operands);
        Assertions.assertEquals(-2.0, result, 0.001);
    }

    @Test
    public void testSubtractionWithZero() {
        Subtraction subtraction = new Subtraction();
        List<Double> operands = Arrays.asList(5.0, 0.0);
        double result = subtraction.calculate(operands);
        Assertions.assertEquals(5.0, result, 0.001);
    }
}
