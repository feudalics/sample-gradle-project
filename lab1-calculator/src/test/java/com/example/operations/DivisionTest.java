package com.example.operations;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

public class DivisionTest {

    @Test
    public void testDivisionWithPositiveNumbers() {
        Division division = new Division();
        List<Double> operands = Arrays.asList(10.0, 2.0);
        double result = division.calculate(operands);
        Assertions.assertEquals(5.0, result, 0.001);
    }

    @Test
    public void testDivisionWithNegativeNumbers() {
        Division division = new Division();
        List<Double> operands = Arrays.asList(-10.0, -2.0);
        double result = division.calculate(operands);
        Assertions.assertEquals(5.0, result, 0.001);
    }

    @Test
    public void testDivisionByZero() {
        Division division = new Division();
        List<Double> operands = Arrays.asList(5.0, 0.0);
        try {
            division.calculate(operands);
            Assertions.fail("Expected an ArithmeticException for division by zero.");
        } catch (ArithmeticException ignored) {
            Assertions.assertTrue(true);
        }
    }
}
