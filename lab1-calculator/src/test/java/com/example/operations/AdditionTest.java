package com.example.operations;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

public class AdditionTest {

    @Test
    public void testAdditionWithPositiveNumbers() {
        Addition addition = new Addition();
        List<Double> operands = Arrays.asList(5.0, 3.0, 2.2);
        double result = addition.calculate(operands);
        Assertions.assertEquals(10.2, result, 0.001);
    }

    @Test
    public void testAdditionWithNegativeNumbers() {
        Addition addition = new Addition();
        List<Double> operands = Arrays.asList(-5.0, -3.0);
        double result = addition.calculate(operands);
        Assertions.assertEquals(-8.0, result, 0.001);
    }

    @Test
    public void testAdditionWithZero() {
        Addition addition = new Addition();
        List<Double> operands = Arrays.asList(0.0, 3.0);
        double result = addition.calculate(operands);
        Assertions.assertEquals(3.0, result, 0.001);
    }
}
