package com.example.operations;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

public class MultiplicationTest {

    @Test
    public void testMultiplicationWithPositiveNumbers() {
        Multiplication multiplication = new Multiplication();
        List<Double> operands = Arrays.asList(5.0, 3.0);
        double result = multiplication.calculate(operands);
        Assertions.assertEquals(15.0, result, 0.001);
    }

    @Test
    public void testMultiplicationWithNegativeNumbers() {
        Multiplication multiplication = new Multiplication();
        List<Double> operands = Arrays.asList(-5.0, -3.0);
        double result = multiplication.calculate(operands);
        Assertions.assertEquals(15.0, result, 0.001);
    }

    @Test
    public void testMultiplicationWithZero() {
        Multiplication multiplication = new Multiplication();
        List<Double> operands = Arrays.asList(0.0, 3.0);
        double result = multiplication.calculate(operands);
        Assertions.assertEquals(0.0, result, 0.001);
    }
}
